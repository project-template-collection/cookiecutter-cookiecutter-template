import filecmp
import os
import pytest

__location__ = os.path.realpath(
    os.path.join(os.getcwd(), os.path.dirname(__file__)))

def test_bake_project(cookies):
    result = cookies.bake(extra_context={'project_slug': 'role_some_test'})

    assert result.exit_code == 0
    assert result.exception is None
    assert result.project.basename == 'role_some_test'
    assert result.project.isdir()

# {% raw %}{{ cookiecutter.project_name}}{% endraw %} Projects


## Usage

```bash
{% raw %}
git clone {{ cookiecutter.project_git.ssh }}
{% endraw %}
```

## Development

```bash
virtualenv -p python3 ~/venvs/develop-{% raw %}{{ cookiecutter.project_name}}{% endraw %}
source ~/venvs/develop-{% raw %}{{ cookiecutter.project_name}}{% endraw %}/bin/activate
pip install -r requirements.txt
```

### Testing


### Releasing

Must be executed from the ``develop`` branch.

```bash
pre-commit uninstall \
    && bumpversion --tag release \
    && git checkout master && git merge develop && git checkout develop \
    && bumpversion --no-tag patch \
    && git push origin master --tags \
    && git push origin develop \
    && pre-commit install
```

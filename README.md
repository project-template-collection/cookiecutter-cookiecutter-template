# Template for new Coookiecutter Templates


## Usage

```bash
virtualenv -p python3 ~/venvs/cookiecutter
source ~/venvs/cookiecutter/bin/activate
pip install cookiecutte>=1.6.0
```

```bash
cookiecutter https://gitlab.com/project-template-collection/cookiecutter-cookiecutter-template.git --checkout v0.0.1.dev
```

```bash
cookiecutter --no-input https://gitlab.com/project-template-collection/cookiecutter-cookiecutter-template.git project_name="Docker Compose"
```

## Development

For Development you need some dependencies listed in the ``requirements.txt``.

```bash
virtualenv -p python3 ~/venvs/develop-cookiecutter
source ~/venvs/develop-cookiecutter/bin/activate
pip install -r requirements.txt
```

### Testing

```bash
py.test -v tests/*
```

### Releasing

Must be executed from the ``develop`` branch.

```bash
pre-commit uninstall \
    && bumpversion --tag release \
    && git checkout master && git merge develop && git checkout develop \
    && bumpversion --no-tag patch \
    && git push origin master --tags \
    && git push origin develop \
    && pre-commit install
```
